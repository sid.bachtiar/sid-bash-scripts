#!/usr/bin/env bash

TOMORROW_DATE=$(date --date=tomorrow +%Y-%m-%d)
TOMORROW_DOW=$(date --date=tomorrow +%u)

HOLIDAY=$(cat holidays.txt | grep $TOMORROW_DATE)

# if tomorrow is Saturday or Sunday or there is a holiday
if [ $TOMORROW_DOW == 6 ] || [ $TOMORROW_DOW == 7 ] || [ -n "${HOLIDAY// /}" ]; then
   if [ $TOMORROW_DOW == 6 ] || [ $TOMORROW_DOW == 7 ]; then
      echo "Tomorrow is weekend."
   else
      echo "Tomorrow $HOLIDAY."
   fi
else
   echo "Tomorrow is neither weekend nor holiday."
fi
