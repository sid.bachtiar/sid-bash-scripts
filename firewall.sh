#!/usr/bin/env bash

# A starting point for firewall script using iptables
# Add this to /etc/rc.local to make this script executed on every start up otherwise iptables is resetted on startup

iptables -F
iptables -X

#  Allow all loopback (lo0) traffic and drop all traffic to 127/8 that doesn't use lo0
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -d 127.0.0.0/8 -j REJECT

#  Accept all established inbound connections
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#  Allow all outbound traffic - you can modify this to only allow certain traffic
iptables -A OUTPUT -j ACCEPT

iptables -A INPUT -p tcp --dport 22 -j ACCEPT    # Allow SSH
iptables -A INPUT -p tcp --dport 443 -j ACCEPT   # Allow HTTPS
iptables -A INPUT -p tcp --dport 80 -j ACCEPT    # Allow HTTP

#DISABLE PING FLOOD
iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j DROP

#  Allow ping
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

#  Drop all other inbound - default deny unless explicitly allowed policy
/sbin/iptables -A INPUT -j DROP
/sbin/iptables -A FORWARD -j DROP

/sbin/iptables -nvL
