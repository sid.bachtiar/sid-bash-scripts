#!/usr/bin/env bash

# 
# Find top 10 biggest PHP files excluding vendor and tmp directories
#

find . -type f \( ! -path "*/tmp/*" \) \( ! -path "*/vendor/*" -prune \) -name "*.php" -exec du {} \; | sort -n -r | head -n 10